'use strict';

var server = require('server');

server.get('PageNotFound', function (req, res, next) {
    res.setStatusCode(404);
    next();
});

module.exports = server.exports();